variable "gitlabSecret" {}

variable "cluster_name" {}

variable "base_domain" {}

variable "repo_url" {}

variable "prod_asg_min_size" {}

variable "prod_asg_desired_capacity" {}

variable "prod_on_demand_base_capacity" {}

variable "prod_asg_max_size" {}

variable "prod_root_volume_size" {}

variable "prod_on_demand_percentage" {}

variable "dev_asg_min_size" {}

variable "dev_asg_desired_capacity" {}

variable "dev_on_demand_base_capacity" {}

variable "dev_on_demand_percentage" {}

variable "dev_asg_max_size" {}

variable "dev_root_volume_size" {}

variable "prod_override_instance_types" {}

variable "dev_override_instance_types" {}

variable "target_revision" {}
