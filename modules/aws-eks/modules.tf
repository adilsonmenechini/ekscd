module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.64.0"

  name                 = format("vpc-%s", var.cluster_name)
  cidr                 = "10.14.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  public_subnets       = ["10.14.1.0/24", "10.14.2.0/24", "10.14.3.0/24"]
  private_subnets      = ["10.14.10.0/24", "10.14.20.0/24", "10.14.30.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "1"
  }
}

module "eks" {
  depends_on      = [module.vpc]
  source          = "terraform-aws-modules/eks/aws"
  subnets         = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id
  enable_irsa     = true
  cluster_name    = var.cluster_name
  cluster_version = "1.19"

  worker_groups_launch_template = [
    {
      name                    = "Devolp"
      override_instance_types = var.dev_override_instance_types
      root_encrypted          = true
      asg_force_delete        = true
      public_ip               = true

      root_volume_size                         = var.dev_root_volume_size
      asg_min_size                             = var.dev_asg_min_size
      asg_desired_capacity                     = var.dev_asg_desired_capacity
      on_demand_percentage_above_base_capacity = var.dev_on_demand_percentage
      on_demand_base_capacity                  = var.dev_on_demand_base_capacity
      asg_max_size                             = var.dev_asg_max_size


      enabled_metrics    = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity"]
      kubelet_extra_args = "--node-labels=worker=devolp --node-labels=node.kubernetes.io/lifecycle=`curl -s http://169.254.169.254/latest/meta-data/instance-life-cycle`"

      tags = [
        {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "false"
          "value"               = "true"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/${var.cluster_name}"
          "propagate_at_launch" = "false"
          "value"               = "true"
        }
      ]
    },
    {
      name                    = "Production"
      override_instance_types = var.prod_override_instance_types
      root_encrypted          = true
      asg_force_delete        = true
      public_ip               = true

      root_volume_size                         = var.prod_root_volume_size
      asg_min_size                             = var.prod_asg_min_size
      asg_desired_capacity                     = var.prod_asg_desired_capacity
      on_demand_percentage_above_base_capacity = var.prod_on_demand_percentage
      on_demand_base_capacity                  = var.prod_on_demand_base_capacity
      asg_max_size                             = var.prod_asg_max_size

      enabled_metrics    = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity"]
      kubelet_extra_args = "--node-labels=worker=production --node-labels=node.kubernetes.io/lifecycle=`curl -s http://169.254.169.254/latest/meta-data/instance-life-cycle`"

      tags = [
        {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "false"
          "value"               = "true"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/${var.cluster_name}"
          "propagate_at_launch" = "false"
          "value"               = "true"
        }
      ]
    },
  ]
}

module "argocd" {
  source = "../argo-cd"

  target_revision    = var.target_revision
  repo_url           = var.repo_url
  cluster_name       = var.cluster_name
  base_domain        = var.base_domain
  gitlabSecret       = var.gitlabSecret
  aws_default_region = data.aws_region.current.name
  cert_manager       = module.cert_manager.this_iam_role_arn
  role_asg           = module.cluster-autoscaler.this_iam_role_arn
  role_runner        = module.gitlabrunner.this_iam_role_arn
  role_vault         = module.vault.this_iam_role_arn

  loki = {
    bucket_name = aws_s3_bucket.loki.id,
  }
  depends_on = [
    module.eks, null_resource.kubectl, module.cluster-autoscaler, module.cert_manager,
  ]
}