module "gitlabrunner" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "3.6.0"
  create_role                   = true
  number_of_role_policy_arns    = 1
  role_name                     = format("gitlabrunner-%s", var.cluster_name)
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.gitlabrunner.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:gitlabrunner:gitlabrunner-gitlab-runner"]
}

resource "aws_iam_policy" "gitlabrunner" {
  depends_on  = [module.vpc, module.eks]
  name_prefix = "gitlabrunner"
  description = "EKS gitlabrunner policy for cluster ${module.eks.cluster_id}"
  policy      = data.aws_iam_policy_document.gitlabrunner.json
}

data "aws_iam_policy_document" "gitlabrunner" {
  statement {
    sid = "GitLabEKSPolicy"
    actions = [
      "autoscaling:CreateAutoScalingGroup",
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeScalingActivities",
      "autoscaling:UpdateAutoScalingGroup",
      "autoscaling:CreateLaunchConfiguration",
      "autoscaling:DescribeLaunchConfigurations",
      "cloudformation:CreateStack",
      "cloudformation:DescribeStacks",
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:RevokeSecurityGroupIngress",
      "ec2:CreateSecurityGroup",
      "ec2:createTags",
      "ec2:DescribeImages",
      "ec2:DescribeKeyPairs",
      "ec2:DescribeRegions",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeVpcs",
      "eks:CreateCluster",
      "eks:DescribeCluster",
      "iam:AddRoleToInstanceProfile",
      "iam:AttachRolePolicy",
      "iam:CreateRole",
      "iam:CreateInstanceProfile",
      "iam:CreateServiceLinkedRole",
      "iam:GetRole",
      "iam:ListRoles",
      "iam:ListAttachedRolePolicies", # Added per https://gitlab.com/gitlab-org/gitlab/-/issues/232960
      "iam:PassRole",
      "ssm:GetParameters"
    ]
    resources = ["*"]
    effect    = "Allow"
  }
}
