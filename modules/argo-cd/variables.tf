variable "gitlabSecret" {}

variable "base_domain" {}

variable "repo_url" {}

variable "cluster_name" {}

variable "aws_default_region" {}

variable "cert_manager" {}

variable "role_runner" {}

variable "target_revision" {}

variable "role_asg" {}

variable "role_vault" {}

variable "loki" {
  description = "Loki settings"
  type        = any
  default     = {}
}

variable "services_values_overrides" {
  description = "Extra value files content for the App of Apps"
  type        = list(string)
  default     = []
}

variable "app_of_apps_values_overrides" {
  description = "Extra value files content for the App of Apps"
  type        = list(string)
  default     = []
}

variable "extra_apps" {
  description = "Extra applications to deploy."
  type        = list(any)
  default     = []
}