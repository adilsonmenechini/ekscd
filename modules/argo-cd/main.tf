locals {
  loki_defaults = {
    bucket_name = ""
  }
  loki = merge(
    local.loki_defaults,
    var.loki,
  )
}

resource "helm_release" "argocd" {
  name              = "argocd"
  chart             = "${path.module}/../../argocd/tools/argocd"
  namespace         = "argocd"
  dependency_update = true
  create_namespace  = true
  timeout           = 10800

  values = [
    file("${path.module}/../../argocd/tools/argocd/values.yaml"),
    <<EOT
    argo-cd:
      configs:
        secret:
          gitlabSecret: ${var.gitlabSecret}
      server:
        config:
            repositories: |
              - url: ${var.repo_url}
                sshPrivateKeySecret:
                  name: argocd-secret
                  key: webhook.github.secret
    EOT
  ]
}

resource "helm_release" "services" {
  depends_on        = [helm_release.argocd]
  name              = "services"
  chart             = "${path.module}/../../argocd/tools/services"
  namespace         = "argocd"
  dependency_update = true
  create_namespace  = true

  values = concat([
    templatefile("${path.module}/../../argocd/tools/services/values.tmpl.yaml",
      {
        target_revision    = var.target_revision
        repo_url           = var.repo_url
        cluster_name       = var.cluster_name
        base_domain        = var.base_domain
        gitlabSecret       = var.gitlabSecret
        extra_apps         = var.extra_apps
        aws_default_region = var.aws_default_region
        cert_manager       = var.cert_manager
        role_asg           = var.role_asg
        role_runner        = var.role_runner
        role_vault         = var.role_vault
        loki               = local.loki
      }
    )],
    var.app_of_apps_values_overrides,
  )
}