# EKS ArgoCD

Project GitOps witch  modules:

- EKS AWS
  - [x] Module eks 
  - [x] Moule vpc
  - [x] Module iam
  - [x] Backend S3
  - [x] Loki S3
  

- ARGOCD
  - [ ] Tools
    - [x] Traefik
    - [x] Cert-manager
    - [x] Auto-scaling
    - [x] Gitlab Runner
    - [x] Prometheus-operator
    - [x] Loki
    - [ ] Vault
