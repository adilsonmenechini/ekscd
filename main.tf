provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "eks-tfstate-trinovati"
    key    = "ekscd/terraform.tfstate"
    region = "us-west-2"
  }
}

module "cluster" {
  source = "./modules/aws-eks"

  gitlabSecret    = var.gitlabSecret
  cluster_name    = "dev-trinovati"
  base_domain     = "trinovati.cf"
  repo_url        = "git@gitlab.com:adilsonmenechini/ekscd.git"
  target_revision = "HEAD"

  prod_override_instance_types = ["t3.medium", "t3.large", "m4.large"]
  prod_asg_min_size            = "1"
  prod_asg_desired_capacity    = "1"
  prod_on_demand_base_capacity = "1"
  prod_asg_max_size            = "5"
  prod_on_demand_percentage    = "30"
  prod_root_volume_size        = "30"

  dev_override_instance_types = ["t3.medium", "t3.large", "m4.large"]
  dev_asg_min_size            = "1"
  dev_asg_desired_capacity    = "1"
  dev_on_demand_base_capacity = "0"
  dev_asg_max_size            = "4"
  dev_on_demand_percentage    = "90"
  dev_root_volume_size        = "10"
}

variable "gitlabSecret" {}